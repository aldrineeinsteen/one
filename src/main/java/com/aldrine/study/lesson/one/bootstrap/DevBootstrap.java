package com.aldrine.study.lesson.one.bootstrap;

import com.aldrine.study.lesson.one.model.Author;
import com.aldrine.study.lesson.one.model.Book;
import com.aldrine.study.lesson.one.model.Publisher;
import com.aldrine.study.lesson.one.repository.AuthorRepository;
import com.aldrine.study.lesson.one.repository.BookRepository;
import com.aldrine.study.lesson.one.repository.PublisherRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;
    private PublisherRepository publisherRepository;

    public DevBootstrap(AuthorRepository authorRepository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        InitData();
    }

    private void InitData() {
        //pub: Harper Collins
        Publisher harper = new Publisher("Harper Collins", "N/A", 1924);
        publisherRepository.save(harper);

        //pub: Amazon
        Publisher amazon = new Publisher("Amazon Pub", "Online", 2012);
        publisherRepository.save(amazon);

        //Aldrine Einsteen
        Author aldrine = new Author("Aldrine", "Einsteen");
        Author evangeline = new Author("Evangeline", "Felicia");
        Book dummy = new Book("Dummy for All", "12353", harper);
        aldrine.getBooks().add(dummy);
        dummy.getAuthors().add(aldrine);
        dummy.getAuthors().add(evangeline);
        authorRepository.save(aldrine);
        authorRepository.save(evangeline);
        bookRepository.save(dummy);

        //Rob
        Author rob = new Author("Rob", "Goseling");
        Book newBook = new Book("J2EE guide", "6654", amazon);
        rob.getBooks().add(newBook);
        authorRepository.save(rob);
        bookRepository.save(newBook);
    }
}
