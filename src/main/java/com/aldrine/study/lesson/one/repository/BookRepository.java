package com.aldrine.study.lesson.one.repository;

import com.aldrine.study.lesson.one.model.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long> {
}
