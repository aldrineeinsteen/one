package com.aldrine.study.lesson.one.repository;

import com.aldrine.study.lesson.one.model.Publisher;
import org.springframework.data.repository.CrudRepository;

public interface PublisherRepository extends CrudRepository<Publisher, Long> {
}
