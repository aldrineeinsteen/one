package com.aldrine.study.lesson.one.repository;

import com.aldrine.study.lesson.one.model.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Long> {
}
