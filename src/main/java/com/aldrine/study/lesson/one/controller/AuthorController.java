package com.aldrine.study.lesson.one.controller;

import com.aldrine.study.lesson.one.repository.AuthorRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AuthorController {

    private AuthorRepository authorRepository;

    public AuthorController(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @RequestMapping("/author")
    public String getAuthors(Model model){
        model.addAttribute("authors", authorRepository.findAll());
        return "author";
    }
}
