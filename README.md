# one
This project is an introduction to spring initializr and common jpa steps.

# Spring init compenents
Below are the components used by the project
1. Spring Web
1. Thymeleaf (instead of jsp)
1. Spring JPA
1. H2 driver and server for embedded application
1. Spring boot actuator

# Steps
Below are the series of steps involved in creating a comprehensive mvc project
1. Create a data (use the package name as `model`)
1. Create a repository (use the package name as `repository`)
1. Create a controller for managing and routing any user action (use the package name as `controller`)
1. Create a thymeleaf template under `resources\templates`. This would be linked to the view created in th controller.

## Points of Interest
* When creating the model for the "Entity'; the constructor(with no argument) is mandatory. 